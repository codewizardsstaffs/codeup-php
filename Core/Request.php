<?php

namespace Core;

use Core\Controller\IControllerFactory;
use Core\Exception\DocumentKeyMismatchException;
use Core\Exception\MethodNotAllowedException;
use Core\Exception\RestableException;
use MongoId;

class Request
{
    /**
     * @var string
     */
    public $method;

    /**
     * @var array
     */
    public $object = [];

    /**
     * @var string
     */
    public $documentKey;

    /**
     * @var string
     */
    public $documentId;

    /**
     * @var string
     */
    public $collection;

    /**
     * @var string
     */
    public $action;

    /**
     * @var string
     */
    public $apiKey;

    public function __construct($method, $parameters, $requestBody)
    {
        $this->method = $method;

        if (isset($parameters[0])) {
            $this->collection = $parameters[0];
        }

        if (isset($parameters[1])) {
            $this->action = $parameters[1];
        }

        if (isset($requestBody['key'])) {
            $this->apiKey = $requestBody['key'];
        }

        if (isset($requestBody['document'])) {
            $document = $requestBody['document'];

            if (isset($document['_id'])) {
                $this->documentId = new MongoId($document['_id']);
            }

            if (isset($document['key'])) {
                $this->documentKey = $document['key'];
            }

            $this->object = $document;
        }
    }

    public function getDocumentId()
    {
        return isset($this->object['_id']) ? new MongoId($this->object['_id']) : null;
    }

    /**
     * @param $controllerFactory IControllerFactory
     * @return Response
     */
    public function getResponse($controllerFactory)
    {
        // todo api apiKey

        $controller = $controllerFactory->getController($this);

        if (!$controller) {
            $response = new Response(EResponseCode::BadRequest);
        } else {
            try {
                $response = $controller->getResponse();
            } catch (MethodNotAllowedException $e) {
                $response = new Response(EResponseCode::MethodNotAllowed, $e->getJson());
            } catch (DocumentKeyMismatchException $e) {
                $response = new Response(EResponseCode::Unauthorized, $e->getJson());
            } catch (RestableException $e) {
                $response = new Response(EResponseCode::BadRequest, $e->getJson());
            }
        }

        return $response;
    }
}