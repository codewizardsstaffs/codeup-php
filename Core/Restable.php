<?php

namespace Core;

use Core\Exception\DocumentKeyMismatchException;
use Core\Exception\InvalidFieldException;
use MongoId;

/**
 * Class Restable
 * @package Core
 * @property int $created
 * @property int $updated
 * @property string $key
 * @property MongoId $_id
 */
abstract class Restable extends MongoObject
{
    public function generateKey()
    {
        $this->key = md5(AppConfig::getInstance()->salt . $this->_id);
    }

    public function assertValid()
    {
        //
    }

    /**
     * @param $json string
     */
    public function useJson($json)
    {
        $this->useArray(json_decode($json));
        $this->postFromDocument();
    }

    /**
     * @param $array
     */
    public function useArray($array)
    {
        $this->__document = $array;

        if ($this->_id) {
            if (!$this->_id instanceof MongoId) {
                $this->_id = new MongoId($this->_id);
            }
        }

        $this->postFromDocument();
    }

    public function postFromDocument()
    {
        // things to do with the post-json object
    }

    /**
     * @return string
     */
    public function toJson()
    {
        return json_encode($this->toDocument(true));
    }

    public function toDocument($convertMongoIds = false)
    {
        return array_merge($this->toDocumentRecursive($this->__document, $convertMongoIds), $this->toDocumentOverride($convertMongoIds));
    }

    public function toDocumentOverride($convertMongoIds = false)
    {
        return [];
    }

    /**
     * @param $array
     * @param boolean $convertMongoIds
     * @return array
     */
    private function toDocumentRecursive($array, $convertMongoIds = false)
    {
        $doc = [];

        if (is_array($array)) {
            foreach ($array as $key => $value) {
                if (is_array($value)) {
                    $doc[$key] = $this->toDocumentRecursive($value, $convertMongoIds);
                } else if ($value instanceof Restable) {
                    $doc[$key] = $value->toDocument($convertMongoIds);
                } else if ($value instanceof MongoId) {
                    $doc[$key] = ($convertMongoIds ? (string)$value : $value);
                } else {
                    $doc[$key] = $value;
                }
            }
        }

        return $doc;
    }

    /**
     * @param $request Request
     * @throws DocumentKeyMismatchException
     * Get the owner of the current document.
     */
    public function assertOwner($request)
    {
        if (!$this->newDocument || $request->documentKey !== $this->key) {
            throw new DocumentKeyMismatchException();
        }
    }

    public static function assertValidDocument($array)
    {
        self::assertValidDocumentRecursive($array);
    }

    public function assertExistingDocumentIsValid()
    {
        self::assertValidDocumentRecursive($this);
    }

    private static function assertValidDocumentRecursive($array)
    {
        foreach ($array as $key => $value) {
            if ($key[0] == '$') {
                throw new InvalidFieldException("Fields starting with $ are not allowed");
            }

            if (strpos($key, '.') !== false) {
                throw new InvalidFieldException("Fields containing . are not allowed");
            }

            if (is_array($value)) {
                self::assertValidDocumentRecursive($value);
            }
        }
    }

    /**
     * @return string
     */
    public function prepareForResponse()
    {
        return json_encode(['document' => $this->toDocument(true)]);
    }

    /**
     * Save this object to the database.
     */
    public function save()
    {
        if ($this->newDocument && !$this->_id && static::$idIsMongoId) {
            $this->_id = new MongoId();
        }

        if ($this->newDocument) {
            $this->onSave();
        } else {
            $this->onUpdate();
        }

        $settings = ['upsert' => 1];

        if (static::$safe) {
            $settings['writeConcern'] = 1;
        }

        MongoRelay::getDB()->selectCollection(static::$collection)->update(['_id' => $this->_id], ['$set' => $this->toDocument()], $settings);

        $this->newDocument = false;
    }
}