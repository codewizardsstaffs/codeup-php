<?php

namespace Core;

use Core\Main\App;
use MongoCursor;
use MongoException;
use MongoId;

/**
 * Class MongoObject
 * @property int|MongoId $_id
 */
abstract class MongoObject
{
    /**
     * @var $this []
     */
    public static $mongo_object_list;
    protected static $collection = 'objects';
    protected static $idIsMongoId = true; // todo merge into single property
    protected static $idIsInt = false; // todo merge into single property
    protected static $safe = false;
    protected $__document = [];

    protected $newDocument = false;

    /**
     * @param mixed $id
     */
    public function __construct($id = null)
    {
        if (is_array($id)) {
            $this->generateParameters($id);
        } else if ($id) {
            $this->load($id);
        } else {
            $this->newDocument = true;
            $this->onCreate();
        }
    }

    /**
     * @param $doc
     */
    public function generateParameters($doc)
    {
        $this->__document = $doc;

        $this->onLoad();
    }

    /**
     * @param $id
     * @throws MongoObjectException
     */
    public function load($id)
    {
        if (static::$idIsMongoId && is_string($id)) {
            try {
                $id = new MongoID($id);
            } catch (MongoException $e) {
                throw new MongoObjectException("Invalid Object ID");
            }
        } else if (static::$idIsInt) {
            $id = (int)$id;
        }

        $doc = MongoRelay::getDB()->selectCollection(static::$collection)->findOne(['_id' => $id]);

        if ($doc) {
            $this->generateParameters($doc);
        } else {
            throw new MongoObjectException("Object not found.");
        }
    }

    /**
     * initiate() and access(): these functions are used for MongoObjects with a VERY small number of documents that are read from frequently.
     * initiate() loads all docs from the collection into the class as their own objects.
     * access() works just like constructing a MongoObject with an ObjectId.
     */
    public static function initiate()
    {
        $class = get_called_class();

        $cursor = MongoRelay::getDB()->selectCollection(static::$collection)->find([], ['_id' => 1]);
        $out = [];

        foreach ($cursor as $doc) {
            $out[(string)$doc['_id']] = new $class($doc['_id']);
        }

        $class::$mongo_object_list = $out;
    }

    /**
     * @param $id
     * @throws MongoObjectException
     * @return $this
     */
    public static function access($id)
    {
        if (isset(static::$mongo_object_list[(string)$id])) {
            return static::$mongo_object_list[(string)$id];
        } else {
            throw new MongoObjectException("MongoObject not initiated or object not found");
        }
    }

    /**
     * @param array $query
     * @return array|null
     */
    public static function getDoc($query = [])
    {
        return MongoRelay::getDB()->selectCollection(static::$collection)->findOne($query);
    }

    /**
     * @param array $query
     * @return $this
     */
    public static function getOne($query = [])
    {
        $cursor = self::getCursor($query);
        $class = get_called_class();

        foreach ($cursor as $doc) {
            return new $class($doc);
        }

        return null;
    }

    /**
     * @param array $query
     * @return MongoCursor
     */
    public static function getCursor($query = [])
    {
        return MongoRelay::getDB()->selectCollection(static::$collection)->find($query);
    }

    /**
     * @param $property
     * @return null
     * Magic method to get the value of a document's field.
     */
    public function __get($property)
    {
        if (isset($this->__document[$property])) {
            return $this->__document[$property];
        } else {
            return null;
        }
    }

    /**
     * @param $property
     * @param $value
     * Magic method to set the value of a document's field.
     */
    public function __set($property, $value)
    {
        $this->__document[$property] = $value;
    }

    /**
     * Delete this object from the database.
     */
    public function delete()
    {
        MongoRelay::getDB()->selectCollection(static::$collection)->remove(['_id' => $this->_id]);

        $this->onDelete();
    }

    /**
     * Save this object to the database.
     */
    public function save()
    {
        if ($this->newDocument && !$this->_id && static::$idIsMongoId) {
            $this->_id = new MongoId();
        }

        if ($this->newDocument) {
            $this->onSave();
        } else {
            $this->onUpdate();
        }

        $settings = ['upsert' => 1];

        if (static::$safe) {
            $settings['writeConcern'] = 1;
        }

        MongoRelay::getDB()->selectCollection(static::$collection)->update(['_id' => $this->_id], ['$set' => $this->__document], $settings);

        $this->newDocument = false;
    }

    /**
     * What to do after an instance is created. Designed to be overridden by a subclass.
     */
    protected function onCreate()
    {

    }

    /**
     * What to do after an instance is loaded from the database. Designed to be overridden by a subclass.
     */
    protected function onLoad()
    {

    }

    /**
     * What to do before an instance is saved to the database for the first time. Designed to be overridden by a subclass.
     */
    protected function onSave()
    {

    }

    /**
     * What to do before an instance is updated on the database. Designed to be overridden by a subclass.
     */
    protected function onUpdate()
    {

    }

    /**
     * What to do after an instance is deleted. Designed to be overridden by a subclass.
     */
    protected function onDelete()
    {

    }
}
