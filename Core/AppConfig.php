<?php
/**
 * Created by PhpStorm.
 * User: fiskie
 * Date: 11/03/15
 * Time: 23:05
 */

namespace Core;

/**
 * Class AppConfig
 * @package Core
 * @property array $mongo
 * @property string $apikey
 * @property string $salt
 */
class AppConfig
{
    private static $instance;
    private $__config;

    public function __get($property)
    {
        return isset($this->__config[$property]) ? $this->__config[$property] : null;
    }

    public function __set($property, $value)
    {
        $this->__config[$property] = $value;
    }

    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new AppConfig();
        }

        return self::$instance;
    }

    private function __construct()
    {
        $this->__config = json_decode(file_get_contents('./Config/api.json'), true);
    }
}