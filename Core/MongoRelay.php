<?php

namespace Core;

use MongoClient;

class MongoRelay
{
    /**
     * @var MongoRelay
     */
    private static $instance;
    public $db;

    private function __construct()
    {
        $config = AppConfig::getInstance();

        $client = new MongoClient($config->mongo['host'] . ':' . $config->mongo['port']);
        $client->connect();

        $this->db = $client->selectDB($config->mongo['database']);
    }

    public static function getDB()
    {
        if (!self::$instance) {
            self::$instance = new MongoRelay();
        }

        return self::$instance->db;
    }
}