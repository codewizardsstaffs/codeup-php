<?php

namespace Core;

use Exception;

/**
 * Class MongoObjectException
 */
class MongoObjectException extends Exception
{
}