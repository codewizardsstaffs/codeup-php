<?php

namespace Core;

class Response
{
    private $code;
    private $output;

    /**
     * @param $code
     * @param string $output
     */
    public function __construct($code, $output = null)
    {
        $this->code = $code;
        $this->output = $output;
    }

    public function dispatch()
    {
        $codes = [
            200 => 'OK',
            201 => 'Created',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            409 => 'Conflict',
            500 => 'Internal Server Error'
        ];

        if (isset($codes[$this->code])) {
            $text = $codes[$this->code];
        } else {
            $text = 'Unknown';
        }

        header("HTTP/1.1 {$this->code} {$text}");
        header('Content-Type: application/json');

        if (is_array($this->output))
            exit(json_encode($this->output));
        else {
            if (!$this->output)
                exit(json_encode(['response' => $this->code]));

            exit($this->output);
        }
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getOutput()
    {
        return $this->output;
    }
}