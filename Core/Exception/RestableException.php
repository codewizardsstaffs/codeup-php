<?php

namespace Core\Exception;

class RestableException extends \Exception
{
    public function getJson()
    {
        return json_encode(['message' => $this->message]);
    }
}