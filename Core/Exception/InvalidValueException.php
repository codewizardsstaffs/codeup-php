<?php

namespace Core\Exception;

class InvalidValueException extends RestableException
{
    private $expected;
    private $given;

    /**
     * @param string $message
     * @param int $expected
     * @param int $given
     */
    public function __construct($message, $expected, $given)
    {
        $this->message = $message;
        $this->expected = $expected;
        $this->given = $given;
    }

    /**
     * @return mixed
     */
    public function getExpected()
    {
        return $this->expected;
    }

    /**
     * @return mixed
     */
    public function getGiven()
    {
        return $this->given;
    }

    public function getJson()
    {
        return ['message' => $this->message, 'expected' => $this->expected, 'given' => $this->given];
    }
}