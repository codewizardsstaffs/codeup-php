<?php

namespace Core\Controller;

use Core\EResponseCode;
use Core\Exception\MethodNotAllowedException;
use Core\MongoObjectException;
use Core\Request;
use Core\Response;
use Core\Restable;
use Exception;
use MongoId;

abstract class RestController extends Controller
{
    /**
     * @var Restable
     */
    protected $restable;

    protected $publicGet = false;

    /**
     * @param $request Request
     */
    public function __construct($request)
    {
        $this->request = $request;
        $this->init();

        try {
            $this->restable->load($this->request->documentId);
        } catch (MongoObjectException $e) {

        }
    }

    /**
     * @return Response
     * @throws MethodNotAllowedException
     * Parse the request and fetch a Response object.
     */
    public function getResponse()
    {
        if ($this->request->action) {
            if ($this->request->action == "delete")
                return $this->delete();

            return $this->performAction();
        }

        try {
            switch ($this->request->method) {
                case 'GET':
                    return $this->get();
                case 'POST':
                    return $this->post();
            }
        } catch (Exception $e) {
            return new Response(EResponseCode::InternalServerError, ['message' => $e->getMessage()]);
        }

        throw new MethodNotAllowedException();
    }

    /**
     * Handling for custom controller actions
     * @return Response
     * @throws MethodNotAllowedException
     */
    public function performAction()
    {
        throw new MethodNotAllowedException();
    }

    /**
     * @return Response
     * @throws \Core\Exception\DocumentKeyMismatchException
     */
    public function get()
    {
        if (!$this->publicGet)
            $this->restable->assertOwner($this->request);

        $this->beforeGet();

        return new Response(EResponseCode::OK, $this->restable->prepareForResponse());
    }

    /**
     * @return Response
     * @throws \Core\Exception\DocumentKeyMismatchException
     */
    public function put()
    {
        $this->beforePut();

        $restable = $this->restable;

        $restable->assertValidDocument($this->request->object);

        $restable->useArray($this->request->object);

        if (!$restable->_id) {
            $restable->_id = new MongoId();
            $restable->generateKey();
        } else {
            $this->restable->assertOwner($this->request);
        }

        $restable->assertValid();

        $restable->save();

        return new Response(EResponseCode::OK, $restable->prepareForResponse());
    }

    /**
     * @return Response
     * @throws \Core\Exception\DocumentKeyMismatchException
     */
    public function post()
    {
        if (!$this->restable->_id) {
            return $this->put();
        }

        $this->restable->assertOwner($this->request);
        $this->restable->assertValidDocument($this->request->object);

        $this->beforePost();

        $existingDoc = $this->restable->toDocument(true);

        foreach ($this->request->object as $key => $value) {
            $existingDoc[$key] = $value;
        }

        $restable = $this->restable;

        $restable->useArray($existingDoc);

        $restable->assertValid();

        $restable->save();

        return new Response(EResponseCode::OK, $restable->prepareForResponse());
    }

    /**
     * @return Response
     * @throws \Core\Exception\DocumentKeyMismatchException
     */
    public function delete()
    {
        $this->restable->assertOwner($this->request);

        $this->beforeDelete();

        $this->restable->delete();

        return new Response(EResponseCode::OK);
    }

    public function beforeGet()
    {

    }

    public function beforePut()
    {

    }

    public function beforePost()
    {

    }

    public function beforeDelete()
    {

    }
}