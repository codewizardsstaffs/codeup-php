<?php

namespace Core\Controller;

use Core\Exception\MethodNotAllowedException;
use Core\Request;
use Core\Response;

abstract class Controller
{
    /**
     * @var Request
     * The current request.
     */
    public $request;

    /**
     * @return mixed
     * Collection-specific controller.
     */
    public abstract function init();

    public function __construct()
    {
        $this->init();
    }

    /**
     * @return Response
     * @throws MethodNotAllowedException
     * Parse the request and fetch a Response object.
     */
    public abstract function getResponse();
}