<?php

namespace Core\Controller;

use Core\Request;

interface IControllerFactory
{
    /**
     * @param $request Request
     * @return Controller|null
     */
    public function getController($request);
}