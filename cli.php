<?php

use CodeUp\Controller\ControllerFactory;
use Core\Request;

include('bootstrap.php');

$method = $argv[1];

$parameters = explode('/', $argv[2]);

array_shift($parameters);

$request = new Request($method, $parameters, json_decode($argv[3], true));

$request->getResponse(new ControllerFactory())->dispatch();