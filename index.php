<?php

use CodeUp\Controller\ControllerFactory;
use Core\Request;

include('bootstrap.php');

$parameters = explode('/', $_SERVER['SCRIPT_URL']);

array_shift($parameters);

$request = new Request($_SERVER['REQUEST_METHOD'], $parameters, json_decode($_REQUEST['payload'], true));

$response = $request->getResponse(new ControllerFactory());

\Core\MongoRelay::getDB()->selectCollection("debug")->save([
    'server' => $_SERVER,
    'request' => $_REQUEST,
    'response' => ['code' => $response->getCode(), 'output' => $response->getOutput()]
]);

$response->dispatch();