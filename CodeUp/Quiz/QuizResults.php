<?php

namespace CodeUp\Quiz;

use Core\Restable;

/**
 * Class QuestionList
 * @package CodeUp\Quiz
 */
class QuizResults extends Restable
{
    public function init()
    {
        $this->collection = 'quizResults';
    }
}