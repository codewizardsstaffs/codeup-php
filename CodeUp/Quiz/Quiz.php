<?php

namespace CodeUp\Quiz;

use Core\Exception\InvalidFieldException;
use Core\Exception\InvalidValueException;
use Core\Restable;
use MongoId;

/**
 * Class QuestionList
 * @package CodeUp\Quiz
 * @property string $title
 * @property string $description
 * @property string $categoryId
 */
class Quiz extends Restable
{
    /**
     * @var QuizCategory
     */
    public $category;

    public function init()
    {
        $this->collection = 'quiz';
    }

    /**
     * @return array
     */
    public function toDocumentOverride($convertMongoIds = false)
    {
        if (!$this->category) {
            return [];
        }

        return ['categoryId' => ($convertMongoIds ? (string)$this->category->_id : $this->category->_id)];
    }

    public function postFromDocument()
    {
        $this->category = new QuizCategory(new MongoId($this->__document['categoryId']));
    }

    public function getQuestions()
    {
        $cursor = $this->getCursor(['quizId' => $this->_id]);

        $questions = [];

        foreach ($cursor as $doc) {
            $question = new Question();

            $question->useArray($doc);

            $questions[] = $question->toDocument(true);
        }

        return $questions;
    }

    public function assertValid()
    {
        if (!$this->category)
            throw new InvalidValueException("A categoryId is required.", "Category ID", $this->category->_id);

        $doc = $this->getOne(['_id' => $this->category->_id]);

        if (!$doc)
            throw new InvalidFieldException("Target quiz category does not exist.");
    }
}