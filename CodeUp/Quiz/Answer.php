<?php
/**
 * Created by PhpStorm.
 * User: fiskie
 * Date: 19/03/15
 * Time: 10:07
 */

namespace CodeUp\Quiz;

use Core\Restable;

/**
 * Class Answer
 * @package CodeUp\Quiz
 * @property string $text
 * @property boolean $correct
 */
class Answer extends Restable
{

}