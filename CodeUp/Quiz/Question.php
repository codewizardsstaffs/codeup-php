<?php

namespace CodeUp\Quiz;

use Core\Exception\InvalidFieldException;
use Core\Exception\InvalidValueException;
use Core\Restable;
use MongoId;

/**
 * Class Question
 * @package CodeUp\Quiz
 * @property string $text
 * @property Answer[] $answers
 * @property MongoId $quizId
 *
 */
class Question extends Restable
{
    /**
     * @var Quiz
     */
    public $quiz;

    protected static $collection = "questions";

    /**
     * @return array
     */
    public function toDocumentOverride($convertMongoIds = false)
    {
        if (!$this->quiz) {
            return [];
        }

        return ['quizId' => ($convertMongoIds ? (string)$this->quiz->_id : $this->quiz->_id)];
    }

    public function postFromDocument()
    {
        $answers = [];

        foreach ($this->answers as $answer) {
            $answers[] = new Answer($answer);
        }

        $this->answers = $answers;

        // $quiz = new Quiz();
        // $quiz->_id = new MongoId($this->__document['quizId']);

        // $this->quiz = $quiz;
    }

    /**
     * @throws InvalidFieldException
     * @throws InvalidValueException
     */
    public function assertValid()
    {
        if (!$this->text)
            throw new InvalidFieldException("A question is required.");

        if (count($this->answers) <= 1)
            throw new InvalidFieldException("At least two answers are required.");

        $correctExists = false;

        foreach ($this->answers as $answer) {
            if ($answer->correct) {
                $correctExists = true;
            }
        }

        if (!$correctExists)
            throw new InvalidFieldException("At least one correct answer was not supplied.");

        // if (!$this->quiz)
        //     throw new InvalidValueException("A quizId is required.", "Quiz ID", $this->quiz->_id);

        // $doc = $this->getDoc(['_id' => $this->quiz->_id]);

        // if (!$doc)
        //     throw new InvalidFieldException("Target quiz does not exist.");
    }
}