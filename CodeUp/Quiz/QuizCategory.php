<?php

namespace CodeUp\Quiz;

use Core\Restable;

/**
 * Class QuizCategory
 * @package CodeUp\Quiz
 * @property string $title
 * @property string $description
 */
class QuizCategory extends Restable
{
    public function init()
    {
        $this->collection = 'quizCategory';
    }
}