<?php

namespace CodeUp\Controller;

use Core\Controller\Controller;
use Core\Controller\IControllerFactory;
use Core\Request;

class ControllerFactory implements IControllerFactory
{
    /**
     * @param $request Request
     * @return Controller|null
     */
    public function getController($request)
    {
        switch ($request->collection) {
            case 'users':
                return new UserController($request);
            case 'quiz':
                return new QuizController($request);
            case 'questions':
                return new QuestionController($request);
            case 'results':
                return new QuizResultsController($request);
            case 'quizCategory':
                return new CategoryController($request);
            default:
                return null;
        }
    }
}