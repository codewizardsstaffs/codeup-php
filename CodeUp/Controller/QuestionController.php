<?php

namespace CodeUp\Controller;

use Core\EResponseCode;
use Core\Response;
use CodeUp\Quiz\Question;
use Core\Controller\RestController;
use Core\Exception\MethodNotAllowedException;

class QuestionController extends RestController
{
    public function init()
    {
        $this->publicGet = true;
        $this->restable = new Question();
    }

    public function performAction() {
        switch ($this->request->action) {
            case 'getRandomPool':
                return $this->getRandomPool();
        }

        throw new MethodNotAllowedException();
    }

    public function getRandomPool() {
        $cursor = Question::getCursor();

        $questions = [];

        foreach ($cursor as $doc) {
            $questions[] = (new Question($doc))->toDocument(true);
        }

        return new Response(EResponseCode::OK, ['questions' => $questions]);
    }
}