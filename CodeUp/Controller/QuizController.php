<?php

namespace CodeUp\Controller;

use CodeUp\Quiz\Quiz;
use Core\Controller\RestController;
use Core\EResponseCode;
use Core\Exception\MethodNotAllowedException;
use Core\Response;

class QuizController extends RestController
{
    public function init()
    {
        $this->publicGet = true;
        $this->restable = new Quiz();
    }

    public function performAction()
    {
        switch ($this->request->action) {
            case 'getQuestions':
                return $this->getQuestions();
            default:
                throw new MethodNotAllowedException();
        }
    }

    public function getQuestions()
    {
        $quiz = new Quiz($this->request->getDocumentId());

        return new Response(EResponseCode::OK, $quiz->getQuestions());
    }
}