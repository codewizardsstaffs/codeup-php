<?php

namespace CodeUp\Controller;

use CodeUp\Quiz\QuizResults;
use Core\Controller\RestController;

class QuizResultsController extends RestController
{
    public function init()
    {
        $this->restable = new QuizResults();
    }
}