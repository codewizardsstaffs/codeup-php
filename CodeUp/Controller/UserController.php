<?php

namespace CodeUp\Controller;

use CodeUp\User\EUserSource;
use CodeUp\User\User;
use Core\Controller\RestController;
use Core\EResponseCode;
use Core\Exception\MethodNotAllowedException;
use Core\MongoRelay;
use Core\Response;
use MongoId;

class UserController extends RestController
{
    public function init()
    {
        $this->restable = new User();
    }

    public function performAction()
    {
        switch ($this->request->action) {
            case 'identify':
                return $this->identify();
            case 'updateScore':
                return $this->updateScore();
            case 'getHighScores':
                return $this->getHighScores();
            case 'getFriends':
                return $this->getFriends();
        }

        throw new MethodNotAllowedException();
    }

    public function identify()
    {
        $user = User::getOne(['userTwitterId' => $this->request->object['id']]);

        if (!$user) {
            $user = new User();

            $user->userTwitterId = $this->request->object['id'];
            $user->name = $this->request->object['name'];
            $user->source = EUserSource::Twitter;
            $user->generateKey();
            $user->save();
        }

        return new Response(EResponseCode::OK, $user->toDocument(true));
    }

    public function updateScore() {
        $user = null;

        try {
            $user = User::getOne(['_id' => new MongoId($this->request->object['_id'])]);
        } catch (\MongoException $e) {
            return new Response(EResponseCode::InternalServerError);
        }

        if ($user->key != $this->request->object['key'])
            return new Response(EResponseCode::Unauthorized);

        $score = 0;

        if (isset($this->request->object['score']))
            $score = $this->request->object['score'];

        if (!$user->dailyScore || $score > $user->dailyScore)
            $user->dailyScore = $score;

        $user->save();

        return new Response(EResponseCode::OK);
    }

    public function getHighScores() {
        $cursor = User::getCursor()->sort(["dailyScore" => -1])->limit(10);

        $users = [];

        foreach ($cursor as $doc) {
            $users[] = (new User($doc))->toDocument(true);
        }

        return new Response(EResponseCode::OK, ['users' => $users]);
    }

    public function getFriends() {
        $cursor = User::getCursor()->limit(10);

        $users = [];

        foreach ($cursor as $doc) {
            $users[] = (new User($doc))->toDocument(true);
        }

        return new Response(EResponseCode::OK, ['users' => $users]);
    }
}