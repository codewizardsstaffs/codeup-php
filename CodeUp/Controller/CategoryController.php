<?php

namespace CodeUp\Controller;

use CodeUp\Quiz\QuizCategory;
use Core\Controller\RestController;

class CategoryController extends RestController
{
    public function init()
    {
        $this->publicGet = true;
        $this->restable = new QuizCategory();
    }
}