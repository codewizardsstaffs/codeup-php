<?php

namespace CodeUp\User;

class EUserSource
{
    const Twitter = 0;
    const Facebook = 1;
    const Google = 2;
}