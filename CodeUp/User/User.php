<?php

namespace CodeUp\User;

use Core\Restable;

/**
 * Class User
 * @package CodeUp\User
 * @property int $source
 * @property string $name
 * @property int $userTwitterId
 * @property int $dailyScore
 */
class User extends Restable
{
    protected static $collection = "users";
}