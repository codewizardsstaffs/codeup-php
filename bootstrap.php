<?php

// Set up class autoloading.
spl_autoload_extensions(".php");
spl_autoload_register('autoload');

function autoload($class)
{
    require str_replace('\\', '/', $class) . '.php';
}
