<?php

use CodeUp\Controller\ControllerFactory;
use CodeUp\Quiz\Answer;
use CodeUp\Quiz\Question;
use CodeUp\Quiz\Quiz;
use CodeUp\Quiz\QuizCategory;
use Core\Request;

class QuestionTest extends PHPUnit_Framework_TestCase
{
    public function testQuestion()
    {
        $quizCategory = new QuizCategory();
        $quizCategory->title = "Quiz Category Title";
        $quizCategory->description = "Quiz category description";

        $request = new Request("PUT", ['quizCategory'], ['document' => $quizCategory->toDocument(true)]);

        $response = $request->getResponse(new ControllerFactory());

        $this->assertEquals(200, $response->getCode());

        $doc = json_decode($response->getOutput(), true);

        $quizCategory->useArray($doc['document']);

        $quiz = new Quiz();
        $quiz->title = "Fact Checking";
        $quiz->description = "Fact Checking 101";
        $quiz->category = $quizCategory;

        $request = new Request("PUT", ['quiz'], ['document' => $quiz->toDocument(true)]);

        $response = $request->getResponse(new ControllerFactory());

        $this->assertEquals(200, $response->getCode());

        $doc = json_decode($response->getOutput(), true);

        $quiz->useArray($doc['document']);

        $question = new Question();
        $question->question = "Which of these is the worst?";

        $answer1 = new Answer();
        $answer1->text = "Java";
        $answer1->correct = true;

        $answer2 = new Answer();
        $answer2->text = "C";

        $question->quiz = $quiz;

        $question->answers = [$answer1, $answer2];

        $request = new Request("PUT", ['questions'], ['document' => $question->toDocument(true)]);

        $response = $request->getResponse(new ControllerFactory());

        $this->assertEquals(200, $response->getCode());

        $request = new Request("GET", ['quiz', 'getQuestions'], ['document' => ['_id' => $quiz->_id]]);

        $response = $request->getResponse(new ControllerFactory());

        $this->assertEquals(200, $response->getCode());

        print_r($response);
    }
}